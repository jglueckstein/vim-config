" bindings
" remap quick escape from insert mode
imap ;; <Esc>

" settings
set number
set relativenumber
highlight Normal guibg=None
